module Helpers
  #agora tenho um método que obtém uma massa de teste que tá dentro da pasta fixtures
  def get_fixture(item)
    YAML.load(File.read(Dir.pwd + "/spec/fixtures/#{item}.yml"), symbolize_names: true)
  end

  def get_thumb(file_name)
    return File.open(File.join(Dir.pwd, "spec/fixtures/images/", file_name), "rb") #argumento rb para obter o conteúdo correcto da imagem
  end

  #aqui eu falo que o método get_fixture ele é "meio" que uma função do módulo pois não é uma classe
  module_function :get_fixture
  module_function :get_thumb
end
