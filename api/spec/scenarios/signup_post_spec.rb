describe "POST/signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@gmail.com", password: "abc123" }
      MongoDB.new.remove_user payload[:email]

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida mensagem de erro" do
      expect(@result.parsed_response["_id"].length).to eql 24 #o id padrão do mongo é composto por 24 caracteres
    end
  end

  context "Usuario ja existe" do
    before(:all) do
      #dado que eu tenho um novo usuário
      payload = { name: "João Gabriel", email: "joao@gmail.com", password: "abc123" }
      MongoDB.new.remove_user payload[:email]

      #e o email desse usuário já foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisição para a rota / signup
      @result = Signup.new.create(payload)
    end

    it "valida status code 409" do
      #então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "valida mensagem de erro" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :(" #o id padrão do mongo é composto por 24 caracteres
    end
  end

  examples = [
    {
      title: "nome em branco",
      payload: { name: "", email: "aline@gmail.com", password: "123abc" },
      code: 412,
      error: "required name",
    },
    {
      title: "sem o campo nome",
      payload: { email: "aline@gmail.com", password: "123abc" },
      code: 412,
      error: "required name",
    },
    {
      title: "email em branco",
      payload: { name: "Aline", email: "", password: "123abc" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem o campo e-mail",
      payload: { name: "Aline", password: "123abc" },
      code: 412,
      error: "required email",
    },
    {
      title: "senha em branco",
      payload: { name: "Aline", email: "aline@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem o campo senha",
      payload: { name: "Aline", email: "aline@gmail.com" },
      code: 412,
      error: "required password",
    },
  ]

  # puts examples.to_json https://www.json2yaml.com

  # #um for each com o argumento e para percorrer por vez cada um dos meus exemplos
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code '#{e[:code]}'" do
        expect(@result.code).to eql e[:code]
      end

      it "valida mensagem de erro '#{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error] #o id padrão do mongo é composto por 24 caracteres
      end

      #name é obrigatório
      #email é obrigatório
      #password é obrigatório
    end
  end
end
