describe "POST / sessions" do
  #dentro deste bloco tenho um contexto, que é o login com sucesso
  context "login com sucesso" do
    before(:all) do
      payload = { email: "betao@gmail.com", password: "abc123" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24 #o id padrão do mongo é composto por 24 caracteres
    end
  end
  # puts result.parsed_response["_id"]
  # puts result.parsed_response #neste caso transforma o o tipo response do httparty para o hash que é padrão do ruby
  # puts result.parsed_response.class
  # puts result.code

  #utiliza a minha massa de teste disponível no arquivo login.yml, symobilize_name é uma chave simbolizada no ruby, symbolize pega os cambos do yml e transforma em símbolo
  # examples = Helpers::get_fixture("login")

  examples = [
    {
      title: "senha incorreta",
      payload: { email: "aline@gmail.com", password: "123abc" },
      code: 401,
      error: "Unauthorized",
    },
    {
      title: "usuario nao existe",
      payload: { email: "a@gmail.com", password: "123abc" },
      code: 401,
      error: "Unauthorized",
    },
    {
      title: "e-mail em branco",
      payload: { email: "", password: "123abc" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem o campo e-mail",
      payload: { password: "123abc" },
      code: 412,
      error: "required email",
    },
    {
      title: "senha em branco",
      payload: { email: "alinec@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem o campo senha",
      payload: { email: "alinec@gmail.com" },
      code: 412,
      error: "required password",
    },
  ]

  # puts examples.to_json https://www.json2yaml.com

  # #um for each com o argumento e para percorrer por vez cada um dos meus exemplos
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code '#{e[:code]}'" do
        expect(@result.code).to eql e[:code]
      end

      it "valida mensagem de erro '#{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error] #o id padrão do mongo é composto por 24 caracteres
      end
    end
  end
end
