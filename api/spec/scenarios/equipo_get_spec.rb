#encoding: utf-8

describe "GET /equipos/{equipos_id}" do
  before(:all) do
    payload = { email: "to@mate.com", password: "abc123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter unico equipo" do
    before(:all) do
      #dado que eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("sanfona.jpg"),
        name: "Sanfona",
        category: "Outros",
        price: "350",
      }

      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      #e eu tenho o id deste equipamento
      equipo = Equipos.new.create(@payload, @user_id)
      @equipo_id = equipo.parsed_response["_id"]

      #quando faço uma requisição get por id
      @result = Equipos.new.find_by_id(@equipo_id, @user_id)
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end
    it "deve retornar o nome" do
      expect(@result.parsed_response).to include("name" => @payload[:name])
    end
  end

  context "equipo nao existe" do
    before(:all) do
      @result = Equipos.new.find_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 404" do
      expect(@result.code).to eql 404
    end
  end
end

describe "GET /equipos/" do
  before(:all) do
    payload = { email: "penelope@charmosa.com", password: "abc123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter uma lista" do
    before(:all) do
      #dado que eu tenho uma lista de equipos
      payloads = [
        {
          thumbnail: Helpers::get_thumb("violino.jpg"),
          name: "Violino classico",
          category: "Cordas",
          price: "250",
        },
        {
          thumbnail: Helpers::get_thumb("violao-nylon.jpg"),
          name: "Violao",
          category: "Outros",
          price: "150",
        },
        {
          thumbnail: Helpers::get_thumb("slash.jpg"),
          name: "Guitarra Slash",
          category: "Cordas",
          price: "350",
        },
      ]

      payloads.each do |payload|
        MongoDB.new.remove_equipo(payload[:name], @user_id)
        Equipos.new.create(payload, @user_id)

        #quando faço uma requisição get para /equipos
        @result = Equipos.new.list(@user_id)
      end
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "deve retornar uma lista" do
      expect(@result.parsed_response).not_to be_empty
      #   puts @result.parsed_response
      #   puts @result.parsed_response.class
    end
  end
end
