#encoding: utf-8
# existe o método force_enconding("ASCII-BBIT") para string com acentuação dentro do código

describe "DELETE /equipos/{equipos_id}" do
  before(:all) do
    payload = { email: "roberto@gmail.com", password: "abc123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter unico equipo" do
    before(:all) do

      #dado que eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumb("clarinete.jpg"),
        name: "Clarinete",
        category: "Outros",
        price: "150",
      }

      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      equipo = Equipos.new.create(@payload, @user_id)
      @equipo_id = equipo.parsed_response["_id"]

      @result = Equipos.new.remove_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end

  #só pra confirmar que ele não encontrou o id
  context "equipo nao existe" do
    before(:all) do
      @result = Equipos.new.remove_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 204" do
      expect(@result.code).to eql 204
    end
  end
end
