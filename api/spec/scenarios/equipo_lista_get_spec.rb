describe "GET /equipos/" do
  before(:all) do
    payload = { email: "penelope@charmosa.com", password: "abc123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter uma lista" do
    before(:all) do
      #dado que eu tenho uma lista de equipos
      payloads = [
        {
          thumbnail: Helpers::get_thumb("violino.jpg"),
          name: "Violino classico",
          category: "Cordas",
          price: "250",
        },
        {
          thumbnail: Helpers::get_thumb("violao-nylon.jpg"),
          name: "Violao",
          category: "Outros",
          price: "150",
        },
        {
          thumbnail: Helpers::get_thumb("slash.jpg"),
          name: "Guitarra Slash",
          category: "Cordas",
          price: "350",
        },
      ]

      payloads.each do |payload|
        MongoDB.new.remove_equipo(payload[:name], @user_id)
        Equipos.new.create(payload, @user_id)

        #quando faço uma requisição get para /equipos
        @result = Equipos.new.list(@user_id)
      end
    end
    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "deve retornar uma lista" do
      expect(@result.parsed_response).not_to be_empty
      #   puts @result.parsed_response
      #   puts @result.parsed_response.class
    end
  end
end
