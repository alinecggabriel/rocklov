describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "ed@gmail.com", password: "abc123" }
    result = Sessions.new.login(payload)
    @ed_id = result.parsed_response["_id"]
  end

  context "solicitar locacao" do
    #dado que a Joana D'arc tem uma "Telecaster" para locação
    before(:all) do
      result = Sessions.new.login({ email: "joe@gmail.com", password: "abc123" })
      joe_id = result.parsed_response["_id"]

      telecaster = {
        thumbnail: Helpers::get_thumb("telecaster.jpg"),
        name: "Telescaster",
        category: "Cordas",
        price: "350",
      }

      MongoDB.new.remove_equipo(telecaster[:name], joe_id)

      result = Equipos.new.create(telecaster, joe_id)
      telecaster_id = result.parsed_response["_id"]

      #quando solicito a locação da Telecaster da Joana D'arc
      @result = Equipos.new.booking(telecaster_id, @ed_id)
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
