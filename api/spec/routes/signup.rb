require_relative "base_api"

class Signup < BaseApi
  def create(payload) # faz a chamada na api e retorna dentro do proprio método
    return self.class.post(
             "/signup",
             body: payload.to_json,
             headers: {
               "Content-Type": "application/json",
             },
           )
  end
end
