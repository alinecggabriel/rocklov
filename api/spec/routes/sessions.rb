require_relative "base_api"

class Sessions < BaseApi
  def login(payload) # faz a chamada na api e retorna dentro do proprio método
    #neste bloco tenho a pré condição de login com sucesso, que é fazer a requisição na API
    #com este código consigo validar o login do usuário, validar o status code 200 e validar o id de usuario se está de acordo com o padrão mongodb
    #argumento multiline para o metodo post
    #então aqui montei uma requisição POST para fazer login na api através do ruby, rspec e httparty
    return self.class.post(
             "/sessions",
             body: payload.to_json,
             headers: {
               "Content-Type": "application/json",
             },
           )
  end
end
