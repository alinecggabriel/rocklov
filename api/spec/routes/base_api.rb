require "httparty"

#esta classe implementa as caracteristicas do HTTParty
class BaseApi
  include HTTParty
  base_uri "http://rocklov-api:3333"
end
