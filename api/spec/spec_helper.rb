require_relative "routes/signup"
require_relative "routes/equipos"
require_relative "routes/sessions"
require_relative "helpers"

require_relative "libs/mongo"

#um recurso do ruby que já vem na liguagem de programação, o ruby consegue criptografar a senha para md5
require "digest/md5"

#este módulo recebe uma senha normal no argumento e retorna ela no formato md5
def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  # semente: este before será o gancho executado uma unica vez antes da execução dos meus cenarios
  config.before(:suite) do
    users = [
      { name: "Roberto Silva", email: "betao@gmail.com", password: to_md5("abc123") },
      { name: "Tomate", email: "to@mate.com", password: to_md5("abc123") },
      { name: "Penelope", email: "penelope@charmosa.com", password: to_md5("abc123") },
      { name: "Roberto Carlos", email: "roberto@gmail.com", password: to_md5("abc123") },
      { name: "Joe Perry", email: "joe@gmail.com", password: to_md5("abc123") },
      { name: "Edward Cullen", email: "ed@gmail.com", password: to_md5("abc123") },

    ]

    MongoDB.new.drop_danger
    MongoDB.new.insert_users(users)
  end
end
