Given("Login com {string} e {string}") do |email, password| #sstep generico e background
  @email = email

  @login_page.open
  @login_page.with(email, password)

  #checkpoint para garantir que estamos no Dashboard
  expect(@dash_page.ondash?).to be true
end

Given("que acesso o formulário de cadastro de anúncios") do
  @dash_page.goto_equipo_form
end

Given("que eu tenho o seguinte equipamento:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  @anuncio = table.rows_hash #quando cria uma variavel com @ ela fica disponível em toda a execução do cenário
  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
end

When("submeto o cadastro deste item") do
  @equipos_page.create(@anuncio)
end

Then("devo ver este item no meu Dashboard") do
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome]
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Then('vejo a mensagem de alerta: {string}"') do |string|
  pending # Write code here that turns the phrase above into concrete actions
end

Then("deve conter a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to have_text expect_alert #have_text é contem!
end

#remover anuncios

Given("que eu tenho um anuncio idesejado") do |table|
  user_id = page.execute_script("return localStorage.getItem('user')")
  log user_id

  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images/", table.rows_hash[:thumb]), "rb")

  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }
  EquiposService.new.create(@equipo, user_id)

  visit current_path
end

When("eu solicito a exclusão deste item") do
  @dash_page.request_removal(@equipo[:name])
  sleep 1 #think time
end

When("confirmo a exclusão") do
  @dash_page.confirm_removal
end

When("não confirmo a solicitação") do
  @dash_page.cancel_removal
end

Then("não devo ver esse item no meu Dashboard") do
  expect(
    @dash_page.has_no_equipo?(@equipo[:name])
  ).to be true
end

Then("esse item deve permanecer no meu Dashboard") do
  expect(@dash_page.equipo_list).to have_content @equipo[:name]
end
