Given("que acesso a página de cadastro") do
  visit "/signup"
end

When("submeto o seguinte formulário de cadastro:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  # log table.hashes
  user = table.hashes.first
  MongoDB.new.remove_user user[:email]
  @signup_page.create(user)
end

# When('submeto um e-mail já cadastrado:') do |table|
#     # table is a Cucumber::MultilineArgument::DataTable

#     log table.hashes
#     user = table.hashes.first

#     log user

#     find("#fullName").set user[:nome]
#     find("#email").set user[:email]
#     find("#password").set user[:senha]

#     click_button "Cadastrar"
# end

# Then('vejo a mensagem de alerta: Email já cadastrado.') do
#     alert_error = find(".alert-error")
#     expect(alert_error.text).to eql "🤭 Email já cadastrado."
# end
