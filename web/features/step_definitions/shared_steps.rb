Then("sou redirecionado para o Dashboard") do
  expect(@dash_page.ondash?).to be true
  #find(".dashboard")
end

Then("vejo a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to eql expect_alert
end
