Feature: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Scenario: Login do usuário

        Given que acesso a página principal
        When submeto minhas credenciais com "alinec@gmail.com" e "abc123"
        Then sou redirecionado para o Dashboard

    @login_inv
    Scenario Outline: Tentativa de login
        Given que acesso a página principal
        When submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Then vejo a mensagem de alerta: "<mensagem_output>"

            Examples:
            | email_input      | senha_input | mensagem_output                  |
            | alinec@gmail.com | psd123      | Usuário e/ou senha inválidos.    |
            | pe@gmail.com     | abc123      | Usuário e/ou senha inválidos.    |
            | fernan#gmail.com | abc123      | Oops. Informe um email válido!   |
            |                  | abc123      | Oops. Informe um email válido!   |
            | pedro@gmail.com  |             | Oops. Informe sua senha secreta! |
