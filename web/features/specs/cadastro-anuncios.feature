Feature: Cadastro de Anúncios
    Sendo usuário cadastrado no RockLov que possui equipamentos musicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibiliza-los para locação

    Background: Login
        * Login com "pedro@gmail.com" e "abc123"

    Scenario: Novo equipo
        Given que acesso o formulário de cadastro de anúncios
        And que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg  |
            | nome      | Fender Stratto |
            | categoria | Cordas         |
            | preco     | 200            |
        When submeto o cadastro deste item
        Then devo ver este item no meu Dashboard

    Scenario Outline: Tentativa de cadastro de anúncios
        Given que acesso o formulário de cadastro de anúncios
        And que eu tenho o seguinte equipamento:
            | thumb     | <foto>        |
            | nome      | <nome_equipo> |
            | categoria | <categoria>   |
            | preco     | <preco>       |
        When submeto o cadastro deste item
        Then deve conter a mensagem de alerta: "<mensagem_output>"

            Examples:
            | foto          | nome_equipo       | categoria | preco | mensagem_output                      |
            |               | Violão de Nylon   | Cordas    | 150   | Adicione uma foto no seu anúncio!    |
            | clarinete.jpg |                   | Outros    | 150   | Informe a descrição do anúncio!      |
            | mic.jpg       | Microphone Shure  |           | 100   | Informe a categoria                  |
            | trompete.jpg  | Trompete Clássico | Outros    |       | Informe o valor da diária            |
            | conga.jpg     | Conga             | Outros    | abc   | O valor da diária deve ser numérico! |
            | conga.jpg     | Conga             | Outros    | 100a  | O valor da diária deve ser numérico! |
            
