Feature: Remover Anúncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anuncio
    Para que eu possa manter o meu dashboard atualizado

Background: Login
        * Login com "fernando@gmail.com" e "abc123"
        
    Scenario: Remover um anúncio
        Given que eu tenho um anuncio idesejado
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 300            |
        When eu solicito a exclusão deste item
        And confirmo a exclusão
        Then não devo ver esse item no meu Dashboard

    Scenario: Desistir da exclusão
        Given que eu tenho um anuncio idesejado
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros    |
            | preco     | 100       |
        When eu solicito a exclusão deste item
        But não confirmo a solicitação
        Then esse item deve permanecer no meu Dashboard

