Feature: Receber pedido de locação
    Sendo um anunciante que possui equipamentos cadastrados
    Desejo receber pedidos de locação
    Para que eu possa decidir se quero aprová-los ou recusá-los
    
    @temp
    Scenario: Receber pedido

        Given que meu perfil de anunciante é "joao@anunciante.com" e "abc123"
        And que eu tenho o seguinte equipamento cadastrado:
            | thumb     | sanfona.jpg |
            | nome      | Sanfona     |
            | categoria | Teclas      |
            | preco     | 400         |
        And acesso o meu dashboard
        When "maria@locataria.com" e "abc123" solicita a locação desse equipo
        Then devo ver a seguinte mensagem: 
        """
        maria@locataria.com deseja alugar o equipamento: Sanfona em: DATA_ATUAL
        """
        And devo ver os links: "ACEITAR" E "REJEITAR" no pedido