class LoginPage
  include Capybara::DSL

  def open
    visit "/"
  end

  def with(email, password) #neste caso passo a ter uma funcionalidade e não só um mapeamento dos elementos
    find("input[placeholder='Seu email']").set email
    find("input[type=password]").set password
    click_button "Entrar"
  end
end
