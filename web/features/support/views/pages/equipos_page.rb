class EquiposPage
  include Capybara::DSL

  def create(equipo)
    page.has_css?("#equipoForm") #ckeckpoint com timeout explicito para garantir que estou no lugar correcto
    # toda vez que encontrar um método com interrogação como o expemplo acima, ele retorna verdadeiro ou falso
    #não é possível utilizar expect dentro do page object, desta forma o item page.has_css?("#equipoForm") pode ser aplicado no lugar do expect

    # expressão condicional: .length é um método que consegue obter a quantidade de itens num array, ou a quantidade de caracteres numa string
    #neste caso se a quantidade de caracteres no thumb for maior que 0, quer dizer que o valor foi preenchido
    upload(equipo[:thumb]) if equipo[:thumb].length > 0

    find("input[placeholder$=equipamento]").set equipo[:nome]
    select_cat(equipo[:categoria]) if equipo[:categoria].length > 0
    find("input[placeholder^=Valor]").set equipo[:preco]

    click_button "Cadastrar"
  end

  def upload(file_name) #encapsulei toda a funcionalidade de fazer upload dentro de um novo método
    # thumb = Dir.pwd + "/features/support/fixtures/images/" + file_name
    thumb = File.join(Dir.pwd, "/features/support/fixtures/images/" + file_name)
    thumb = thumb.tr("/", "\\") if OS.windows?

    find("#thumbnail input[type=file]", visible: false).set thumb
  end

  def select_cat(cat)
    find("#category").find("option", text: cat).select_option
  end
end
