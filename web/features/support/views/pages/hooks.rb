Before do #executa o metodo antes de cada cenario
  @alert = Alert.new
  @login_page = LoginPage.new
  @signup_page = SignupPage.new
  @dash_page = DashPage.new
  @equipos_page = EquiposPage.new

  #page.driver.browser.manage.window.maximize
  page.current_window.resize_to(1440, 900) #definir resolução da janela para teste
end

After do
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")

  #método com 3 argumentos para adicionar um arquivo ao relatorio do Allure
  #o cucumber vai toda vez que executar um cenario vai usar este gancho ao tirar um screeshot temporario e vai anexar ao cenario exetuado
  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),
  )
end
