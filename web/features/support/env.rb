require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"
require "os"

#constante: uma vez que receber o valor, o valor não vai mudar
# RECURSO do ruby chamdo ENV, o ENV é um recurso para eu ter acesso as variáveis de ambiente, neste caso vou utilizar a varíavel de ambiente CONFIG que foi definida no cucumber.yml
CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))

# BROWSER = ENV["BROWSER"]
# if BROWSER == "firefox"
#   @driver = :selenium
# elsif BROWSER == "fire_headless"
#   @driver = :selenium
# elsif BROWSER == "chrome"
#   @driver = :selenium
# else BROWSER == "chrome_headless"
#   @driver = :selenium end

case ENV["BROWSER"]
when "firefox"
  @driver = :selenium
when "fire_headless"
  @driver = :selenium_headless
when "chrome"
  @driver = :selenium_chrome
when "chrome_headless"
  @driver = :selenium_chrome_headless

  Capybara.register_driver :selenium_chrome_headless do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << "--headless"
      opts.args << "--disable-gpu"
      opts.args << "--disable-site-isolation-trials"
      opts.args << "--no-sandbox"
      opts.args << "--disable-dev-shm-usage"
    end
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
  end
else
  raise "Invalid Browser :(" #raise vai estourar uma sessão e não vai executar mais o código
end

Capybara.configure do |config|
  config.default_driver = @driver
  config.app_host = CONFIG["url"]
  config.default_max_wait_time = 50 #o capybara vai aguardar 5 segundos para encontrar o elemento, customiza de acordo com a performance da aplicação, chama-se timeout implicito
end

AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  config.clean_results_directory = true
end
